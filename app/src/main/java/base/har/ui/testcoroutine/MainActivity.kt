package base.har.ui.testcoroutine

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import base.har.R
import base.har.base.BaseActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private var vModal: MainActivityViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addObservers()
        btnLogin.setOnClickListener {
            showLoading("")
            vModal?.loginAPi()

        }

    }

    private fun addObservers() {
        vModal = ViewModelProviders.of(this)[MainActivityViewModel::class.java]


        vModal?.loginLiveData?.observe(this, Observer {
            hideLoading()
            Toasty.success(this,it.message).show()
        })

        vModal?.errorLiveData?.observe(this, Observer {
            hideLoading()
            Toasty.error(this,it.message).show()
        })

    }
}
