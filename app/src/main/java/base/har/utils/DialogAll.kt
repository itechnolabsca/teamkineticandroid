package base.har.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.provider.Settings
import base.har.R
import base.har.cache.PrefNames
import base.har.cache.Prefs
import base.har.ui.SplashActivity


class DialogAll(private val context: Context) {
    var isFirstTime: Boolean? = true

    fun showNoInternetDialog() {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle(context.resources.getString(R.string.app_name))
        alertDialog.setCancelable(false)
        alertDialog.setMessage(context.resources.getString(R.string.internet_connection))
        alertDialog.setPositiveButton(
            context.resources.getString(android.R.string.ok)
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()
    }

    fun showLocationDialog(title: String, message: String, flag: Int) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle(title)
        alertDialog.setCancelable(false)
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(
            context.resources.getString(android.R.string.ok)
        ) { dialog, which ->
            dialog.dismiss()

            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            //                        startActivity(intent);
            //                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            //                        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
            //                        intent.setData(uri);
            (context as Activity).startActivityForResult(intent, 202)
            context.overridePendingTransition(R.anim.slide_left_fast_in, R.anim.slide_left_fast)
        }

        alertDialog.setNegativeButton(
            context.resources.getString(android.R.string.cancel)
        ) { dialog, which ->
            if (flag == KILL_ACTIVITY) {
                (context as Activity).finish()
            }
            dialog.dismiss()
        }
        try {
            alertDialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun clearPrefs() {

        val pushToken = Prefs.with(context).getString(PrefNames.PUSH_TOKEN, "")

        Prefs.with(context).removeAll()
        if (!pushToken.isBlank())
            Prefs.with(context).save(PrefNames.PUSH_TOKEN, pushToken)
    }


    fun showDialog(title: String, message: String, flag: Int) {

        if (flag == LOGIN_DIRECT) {
            clearPrefs()
        }

        val alertDialog = AlertDialog.Builder(context)

        if (!title.isEmpty()) {
            alertDialog.setTitle(title)
        }

        alertDialog.setCancelable(false)

        alertDialog.setMessage(message)

        alertDialog.setPositiveButton(
            context.resources.getString(android.R.string.ok)
        ) { dialog, which ->
            if (flag == LOGIN_DIRECT) {
                val intent = Intent(context, SplashActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)

            }


            dialog.dismiss()
        }

        alertDialog.show()
    }


    val MYBASICINFO: Int? = 101


//    fun dialogLogout() {
//        val builder = android.support.v7.app.AlertDialog.Builder(context)
//        builder.setMessage("Are you sure you want to logout?")
//                .setCancelable(false)
//                .setPositiveButton("Yes") { dialog, id ->
//                    if (InternetCheck.isConnectedToInternet(context)) {
//                        LogoutPresenter(this, context).apiLogout()
//                    }
//                }
//                .setNegativeButton("No") { dialog, id -> dialog.cancel() }
//        val alert = builder.create()
//        alert.show()
//    }


    private fun clearPrefAndOpenLogin(context: Context) {
        var string: String
        string = Prefs.with(context).getString(PrefNames.PUSH_TOKEN, "")
        Prefs.with(context).removeAll()
        Prefs.with(context).save(
            PrefNames
                .PUSH_TOKEN, string
        )
        val intent = Intent(context, SplashActivity::class.java)
        context.startActivity(intent)
        (context as Activity).finishAffinity()
    }


    companion object {

        val KILL_ACTIVITY = 212
        val LOGIN_DIRECT = 213
    }


}
