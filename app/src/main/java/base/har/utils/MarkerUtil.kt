package base.har.utils

import android.content.Context
import android.graphics.*


fun getBitmapMarker(mContext: Context, resourceId: Int, mText: String): Bitmap? {
    try {
        val resources = mContext.getResources()
        val scale = resources.getDisplayMetrics().density
        var bitmap = BitmapFactory.decodeResource(resources, resourceId)

        var bitmapConfig: android.graphics.Bitmap.Config? = bitmap.config

        // set default bitmap config if none
        if (bitmapConfig == null)
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888

        bitmap = bitmap.copy(bitmapConfig, true)

        val canvas = Canvas(bitmap)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.setColor(Color.WHITE)
        paint.setTextSize((14.0f * scale))
        paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY)

        // draw text to the Canvas center
        val bounds = Rect()
        paint.getTextBounds(mText, 0, mText.length, bounds)
        val x = (bitmap.width - bounds.width()) / 2
        val y = (bitmap.height + bounds.height()) / 2

        canvas.drawText(mText, x * scale, y * scale, paint)

        return bitmap

    } catch (e: Exception) {
        return null
    }

}
