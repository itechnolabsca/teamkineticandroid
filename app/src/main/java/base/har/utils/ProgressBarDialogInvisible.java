package base.har.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import base.har.R;
import com.wang.avi.AVLoadingIndicatorView;


public class ProgressBarDialogInvisible {


    public static Dialog dialog;
    private static AVLoadingIndicatorView avi;

    public static void showProgressBar(final Activity activity, String title) {

        if (dialog != null) {
            if (dialog.isShowing()) {
                return;
            }
        }

        try {
            if ("".equalsIgnoreCase(title)) {
                title = "Loading...";
            }
//            title = "Alert";
            dialog = new Dialog(activity,
                    R.style.AppTheme);
//            dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog_AppCompat;

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(
                    android.R.color.transparent);
            WindowManager.LayoutParams layoutParams = dialog.getWindow()
                    .getAttributes();
            layoutParams.dimAmount = .5f;
            dialog.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);


            dialog.setContentView(R.layout.progressbar_dialog);
              avi =  dialog.findViewById(R.id.avi);
            TextView tvProgressText = (TextView) dialog.findViewById(R.id.tvProgressText);

            tvProgressText.setText(title);
            avi.show();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void dismissProgressDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    avi.hide();
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
