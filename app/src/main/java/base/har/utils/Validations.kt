package base.har.utils


import android.content.Context
import android.util.Patterns
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import base.har.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class Validations(private val context: Context) {

    /**
     * check if EditText is Emptyor not
     */
    fun isNotEmpty(et: EditText): Boolean {
        val data = et.text.toString().trim { it <= ' ' }

        if (data.isEmpty()) {

            var hint = ""

            if (et is TextInputEditText) {
                val parent = et.parent.parent as TextInputLayout
                hint = parent.hint!!.toString()
            } else {
                hint = et.hint!!.toString()
            }


            if (hint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                setError(et, context.getString(R.string.pleaseEnter) + " " + hint)
            } else {
                setError(et, context.getString(R.string.pleaseEnter) + " " + hint)
            }

            return false
        }
        return true
    }


    /**
     * check if EditText is Emptyor not
     */
    fun isEmpty(et: EditText): Boolean {
        val data = et.text.toString().trim { it <= ' ' }

        if (data.isEmpty()) {

            var hint = ""

            if (et is TextInputEditText) {
                val parent = et.parent.parent as TextInputLayout
                hint = parent.hint!!.toString()
            } else {
                hint = et.hint!!.toString()
            }


            if (hint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                setError(et, context.getString(R.string.pleaseEnter) + " " + hint)
            } else {
                setError(et, context.getString(R.string.pleaseEnter) + " " + hint)
            }

            return false
        }
        return true
    }

    /**
     * check if EditText is Emptyor not
     */
    fun isEmpty(et: EditText, string: String): Boolean {
        val data = et.text.toString().trim { it <= ' ' }

        if (data.isEmpty()) {

            var hint = ""

            if (et is TextInputEditText) {
                val parent = et.parent.parent as TextInputLayout
                hint = parent.hint!!.toString()
            } else {
                hint = et.hint!!.toString()
            }


            if (hint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                setError(et, string)
            } else {
                setError(et, string)
            }

            return false
        }
        return true
    }




    /**
     * check if TextView is Emptyor not
     */
    fun isNotEmpty(tv: TextView): Boolean {
        val data = tv.text.trim().toString()

        if (data.isEmpty()) {

            if(tv.parent.parent is TextInputLayout) {
                val parent = tv.parent.parent as TextInputLayout
                val hint = parent.hint!!.toString()
                if (hint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                    setError(tv, context.getString(R.string.pleaseEnter) + " " + hint)
                } else {
                    setError(tv, context.getString(R.string.pleaseEnter) + " " + hint)
                }
            }
            else
                setError(tv, context.getString(R.string.pleaseEnter) + " " + tv.hint)



            return false
        }
        return true
    }

    /**
     * check if TextView is Emptyor not
     */
    fun isEmpty(tv: TextView): Boolean {
        val data = tv.text.trim().toString()

        if (data.isEmpty()) {

            if(tv.parent.parent is TextInputLayout) {
                val parent = tv.parent.parent as TextInputLayout
                val hint = parent.hint!!.toString()
                if (hint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                    setError(tv, context.getString(R.string.pleaseEnter) + " " + hint)
                } else {
                    setError(tv, context.getString(R.string.pleaseEnter) + " " + hint)
                }
            }
            else
                setError(tv, context.getString(R.string.pleaseEnter) + " " + tv.hint)



            return false
        }
        return true
    }


    /**
     * Check if Email Address is valid or not
     */
    fun isEmptyValidEmail(et: EditText): Boolean {

        val email = et.text.toString().trim { it <= ' ' }


        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() || isValidNumber2(et)) {

            setError(et, "" + context.getString(R.string.validEmail))
            return false
        }

        return true
    }

    /**
     * Check if Email Address is valid or not
     */
    fun isValidEmail(et: EditText): Boolean {
        if (!isEmpty(et)) {
            return false
        }

        val email = et.text.toString().trim { it <= ' ' }


        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() || isValidNumber2(et)) {

            setError(et, "" + context.getString(R.string.validEmail))
            return false
        }

        return true
    }


    /**
     * Check if Password Entered is valid or not
     * checks Password must contain alphanumeric Characters
     */
    fun isValidPassword(et: EditText, string: String): Boolean {
        if (!isEmpty(et, string)) {
            return false
        }
        val password = et.text.toString().trim { it <= ' ' }

        if (password.length > 5) {
            return true
        } else {
            setError(et, context.getString(R.string.validPass))
            return false
        }
    }

    /**
     * Check if Password Entered is valid or not
     * checks Password must contain alphanumeric Characters
     */
    fun isValidConfirmPassword(et: EditText, message: String): Boolean {
        if (!isEmptyPass(et, message)) {
            return false
        }
        return true
    }


    /**
     * check if EditText is Emptyor not
     */
    fun isEmptyPass(et: EditText, message: String): Boolean {
        val data = et.text.toString().trim { it <= ' ' }

        if (data.isEmpty()) {

            var hint = ""
            var parent: TextInputLayout? = null
            if (et is TextInputEditText) {
                parent = et.parent.parent as TextInputLayout
                hint = parent.hint!!.toString()
            } else {
                hint = et.hint!!.toString()
            }

            if (hint.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray().size > 1) {
                if (parent == null)
                    setError(et, context.getString(R.string.please) + " " + message)
                else
                    setError(parent, context.getString(R.string.please) + " " + message)
            } else {
                setError(et, message)
            }

            return false
        }
        return true
    }

    /**
     * check if Username Entered is Valid or not
     */
    fun isValidUsername(et: EditText): Boolean {

        if (!isEmpty(et)) {
            return false
        }
        val userName = et.text.toString().trim { it <= ' ' }

        if (userName.matches("[positionSelected-zA-Z\\s]+".toRegex())) {
            if (Character.isUpperCase(userName[0])) {
                //                if (userName.length() >= 3) {
                var count = 0
                var flagusSuccess = true
                for (i in 1 until userName.length) {
                    if (userName[i] == ' ' && userName[i + 1] == ' ') {
                        setError(et, context.getString(R.string.onlyOneSpace))

                        flagusSuccess = false
                    }

                    if (userName[i] == ' ') {
                        count++
                        if (count > 2) {
                            setError(et, context.getString(R.string.validUserName))
                            flagusSuccess = false
                        }
                    }
                }
                if (flagusSuccess) {
                    return true
                }
                //                }
            }
            //            setError(et,"" + context.getString(R.string.firstLetterCap));
            return true
        } else {
            setError(et, context.getString(R.string.validUserName))
            return false
        }
    }

    /**
     * check if Username Entered is Valid or not
     */
    fun isValidUsername(et: EditText, string: String): Boolean {

        if (!isEmpty(et, string)) {
            return false
        }
        val userName = et.text.toString().trim { it <= ' ' }

        if (userName.matches("[positionSelected-zA-Z\\s]+".toRegex())) {
            if (Character.isUpperCase(userName[0])) {
                //                if (userName.length() >= 3) {
                var count = 0
                var flagusSuccess = true
                for (i in 1..userName.length - 1) {
                    if (userName[i] == ' ' && userName[i + 1] == ' ') {
                        setError(et, context.getString(R.string.onlyOneSpace))

                        flagusSuccess = false
                    }

                    if (userName[i] == ' ') {
                        count++
                        if (count > 2) {
                            setError(et, context.getString(R.string.validUserName))
                            flagusSuccess = false
                        }
                    }
                }
                if (flagusSuccess) {
                    return true
                }
                //                }
            }
            //            setError(et,"" + context.getString(R.string.firstLetterCap));
            return true
        } else {
            setError(et, context.getString(R.string.validUserName))
            return false
        }
    }


    fun isTermsAccepted(checkBox: CheckBox, act: Context): Boolean {

        if (!checkBox.isChecked) {
            DialogAll(act)
                .showDialog("", "Please accept the Terms & Conditions by clicking on checkbox.", 0)

            //checkBox.error = "Please accept the Terms & Conditions by clicking on checkbox."
            return false
        }
        //checkBox.error=null

        return true

    }

    fun isEmptyCustom(et: EditText, hint: String): Boolean {
        val data = et.text.toString().trim { it <= ' ' }

        if (data.isEmpty()) {
            setError(et, hint + " " + context.getString(R.string.contactNumber))
            return false
        }
        return true
    }


    /**
     * check if PhoneNumber Entered is Valid or not and
     */
    fun isValidNumber2(et: EditText): Boolean {
        if (!isEmptyCustom(et, "Please")) {
            return false
        }
        val number = et.text.toString().trim { it <= ' ' }

        try {
            if (!number.matches("[0]*[1-9][0-9]*".toRegex())) {
                // et.error = "" + context.getString(R.string.validPhone)
                return false
            } else if (number.length < 6 || number.length > 10) {
                // setError(et, "" + context.getString(R.string.phoneLength))
                return false
            }
            return true
        } catch (ex: NumberFormatException) {
            //  setError(et, "" + context.getString(R.string.validPhone))
            return false
        }

    }


    /**
     * check if PhoneNumber Entered is Valid or not and
     */
    fun isValidPhoneNumber(et: EditText): Boolean {
        if (!isEmptyCustom(et, "Please")) {
            return false
        }
        val number = et.text.toString().trim { it <= ' ' }

        try {
            if (!number.matches("[0]*[1-9][0-9]*".toRegex())) {
                et.error = "" + context.getString(R.string.validPhone)
                return false
            } else if (number.length < 6 || number.length > 10) {
                setError(et, "" + context.getString(R.string.phoneLength))
                return false
            }
            return true
        } catch (ex: NumberFormatException) {
            setError(et, "" + context.getString(R.string.validPhone))
            return false
        }

    }


    /**
     * check if CardNumber Entered is valid or not
     */
    fun isValidCardNumber(et: EditText): Boolean {

        val number = et.text.toString().trim { it <= ' ' }
        try {
            if (!number.matches("[0]*[1-9][0-9]*".toRegex())) {
                setError(et, "Enter valid Card Number")
                return false
            } else if (number.length < 16) {
                setError(et, "CardNumber must contain atleast 16 digits")
                return false
            }
            return true
        } catch (ex: NumberFormatException) {
            setError(et, "Enter valid Number")
            return false
        }

    }

    /**
     * Checks if Expiry Date Entered is valid or not
     */
    fun isValidExpiryDate(et: EditText): Boolean {

        val expiryDate = et.text.toString().trim { it <= ' ' }
        val dateFormat = SimpleDateFormat("yyyy/MM/dd")
        val date1 = Date()
        val date = dateFormat.format(date1)
        println("date.." + date)
        if (!expiryDate.matches("^\\d{4}/\\d{2}/\\d{2}$".toRegex())) {
            setError(et, "Date Format is YYYY/MM/dd")
            return false
        } else if (expiryDate == date) {
            setError(et, "Expiry date can't be the Current date")
            return false
        } else if (!expiryDate.matches("(((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01]))".toRegex())) {
            setError(et, "Enter valid date")
            return false
        }
        return true
    }

    /**
     * checks if User entered Valid CVV Number
     */

    fun isValidCVV(cvv: String, et: EditText): Boolean {


        try {
            val num = Integer.parseInt(cvv)

            if (!cvv.matches("[0]*[1-9][0-9]*".toRegex())) {
                setError(et, "Enter valid CVV Number")
                return false
            } else if (cvv.length != 3) {
                setError(et, "CVV Number must contain 3 digits")
                return false
            }
            return true
        } catch (ex: NumberFormatException) {
            setError(et, "Enter valid CVV Number")
            return false
        }

    }

    /**
     * checks if user Entered valid PinCode Number
     */

    fun isValidZIPCode(et: EditText): Boolean {
        val zipCode = et.text.toString().trim { it <= ' ' }

        if (!isEmpty(et)) {
            return false
        }

        return true

    }

    /**
     * checks if any EditText contains error if yes then return focus to it
     */
    fun setError(et: View, s: String) {

        if(et is EditText) {
            et.error = s
            et.requestFocus()
        }
        else if(et is TextInputLayout)
        {
            et.error = s
            et.requestFocus()
        }
    }

    /**
     * checks if any TextView contains error if yes then return focus to it
     */
    fun setError(et: TextView, s: String) {

        et.error = s
        et.requestFocus()
    }

    /**
     * checks if password matches or not
     */
    fun confirmPassword(etNewPass: EditText, etConfirmPass: EditText): Boolean {

        val oldPassword = etNewPass.text.toString().trim { it <= ' ' }
        val newPassword = etConfirmPass.text.toString().trim { it <= ' ' }

        if (!oldPassword.matches(newPassword.toRegex())) {
            etNewPass.error = "" + context.getString(R.string.passMatch)
            etConfirmPass.error = "" + context.getString(R.string.passMatch)
            return false
        }
        return true
    }


    fun isValidImage(outputFile: File?): Boolean {

        if (outputFile == null) {
            DialogAll(context).showDialog("", context.getString(R.string.addImage), 0)
        } else
            return true
        return false
    }

    fun isValidWebUrl(etWebsite:EditText):Boolean{
        var website = etWebsite.text.toString()
        val pattern:Pattern = Patterns.WEB_URL
        val m:Matcher = pattern.matcher(website.toLowerCase())
        val b:Boolean = m.matches()
        if(!b)
        {
            etWebsite.error = ""+context.getString(R.string.validURL)
        }
        return b
    }

    companion object {

        fun capFirstLetterName(name: String): String {
            return (name[0] + "").toUpperCase() + name.substring(1)
        }
    }
}