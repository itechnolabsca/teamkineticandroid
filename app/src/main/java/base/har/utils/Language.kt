package base.har.utils

import android.content.Context
import base.har.cache.Prefs


var SELECTED_LANGUAGE="selectedLanguage"


var LANGUAGE_ENGLISH=101
var LANGUAGE_HEBREW=102

fun getLanguage(context: Context): Int {
    return Prefs.with(context).getInt(
        SELECTED_LANGUAGE,
        LANGUAGE_ENGLISH
    )
}