package base.har.retrofit

sealed class Result<out T: Any> {
    data class Success<out T : Any>(val data: T?) : Result<T>()
    data class Error(val exception: ErrorCustom) : Result<Nothing>()
}