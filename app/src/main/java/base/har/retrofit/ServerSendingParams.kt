package base.har.retrofit


object ServerSendingParams {


    // normal phone signup
    val USER_LOGIN = "user_login"
    val EMAIL = "email"
    val PASSWORD: String = "password"
    val LOGIN_TYPE: String="authProvider"
    val PHONE: String="phoneNumber"
    val USERNAME: String="userName"
    val FIRST_NAME="firstName"
    val LAST_NAME="lastName"
    val MIDDLE_NAME="middleName"
    val DOB="dob"

    val PROVIDERID="profileId"
    val PROVIDER="provider"
    val GENDER="gender"

    val OTP: String="Otp"
    val DEVICE_TOKEN = "device_token"
    val DEVICE_TYPE = "device_type"

    val OLD_PASSWORD: String="oldPassword"
    val NEW_PASSWORD: String="password"
    val USERID: String="userId"
    val ID: String="id"


}

//fb
//{
//    "dob": "string",
//    "email": "string",
//    "firstName": "string",
//    "gender": "string",
//    "lastName": "string",
//    "middleName": "string",
//    "phoneNumber": "string",
//    "profileId": "string",
//    "provider": "string",
//    "userName": "string"
//}

//{
//    "dob": "string",
//    "email": "string",
//    "firstName": "string",
//    "gender": "male",
//    "id": 0,
//    "lastName": "string",
//    "middleName": "string",
//    "userName": "string"
//}