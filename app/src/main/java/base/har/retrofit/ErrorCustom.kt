package base.har.retrofit

class ErrorCustom() {

    constructor(statuscode: Int, message: String) : this() {
        this.statusCode = statuscode
        this.message = message
    }

    var statusCode: Int = 0
    var message = ""
    var type = ""
    var error=""


}
