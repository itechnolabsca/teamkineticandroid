package base.har.retrofit

import android.util.Log
import android.util.MalformedJsonException
import base.har.base.BaseDataModel
import base.har.base.BaseViewModel
import com.google.gson.Gson
import io.reactivex.observers.DisposableObserver
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.lang.ref.WeakReference
import java.net.SocketTimeoutException

abstract class CallbackWrapperRx<T>(var view: BaseViewModel) : DisposableObserver<T>() {

    //BaseView is just positionSelected reference of positionSelected View in MVP
    private val weakReference: WeakReference<BaseViewModel> = WeakReference(view)


    protected abstract fun onSuccess(t: T)

    override fun onNext(t: T) {
        //You can return StatusCodes of different cases from your API and handle it here. I usually include these cases on BaseResponse and iherit it from every Response
      if(t is BaseDataModel)
      {
         if( t.statusCode== HTTPStatus.STATUS_SUCCESS200 || t.statusCode== HTTPStatus.STATUS_SUCCESS201 || t.statusCode== HTTPStatus.STATUS_SUCCESS202 || t.statusCode== HTTPStatus.CREATE_ACCOUNT)
             onSuccess(t)
          else
             view.throwError(ErrorCustom(t.statusCode, t.message))
      }
        else if(t is Response<*>)
      {
          if(t.body()==null && t.errorBody()!=null)
          {
              view.throwError(getErrorMessage(t.errorBody()!!))
              return
          }
          if(t.body() is BaseDataModel)
          {
              if( (t.body() as BaseDataModel).statusCode== HTTPStatus.STATUS_SUCCESS200 || (t.body() as BaseDataModel).statusCode== HTTPStatus.STATUS_SUCCESS201)
                  onSuccess(t)
              else
                  view.throwError(
                      ErrorCustom(
                          (t.body() as BaseDataModel).statusCode,
                          (t.body() as BaseDataModel).message
                      )
                  )
          }
              else
              onSuccess(t)
      }
        else
        onSuccess(t)
    }

    override fun onError(e: Throwable) {
        val view = weakReference.get()
        if (e is HttpException) {
            handleError(e)
        } else if (e is MalformedJsonException || e is com.google.gson.stream.MalformedJsonException) {
            view?.onUnknownError(e.localizedMessage)
        } else if (e is SocketTimeoutException) {
            view?.onTimeout()
        } else if (e is IOException) {
            view?.onNetworkError()
        } else {
            view?.onUnknownError(e.message.toString())
        }
    }

    fun handleError(exception: HttpException) {
        val gson = Gson()
        var error = ErrorCustom()
        val adapter = gson.getAdapter(ErrorCustom::class.java)
        try {
            var errorBody = exception.response().errorBody()
            if (exception.response().errorBody() != null)
                error = adapter.fromJson(
                        errorBody?.string())
            Log.e("API ERROR ", error.message)
            error.statusCode=error.statusCode
            // Toasty.error(activity, exampleLogin!!.message)
            view.throwError(error)
        } catch (e: IOException) {
            somethingWentWrong(exception)
            e.printStackTrace()
        }
    }

    private fun somethingWentWrong(exception: HttpException) {
        var error = ErrorCustom()
        error.message = exception.message()
        error.statusCode == exception.code()

        view.throwError(error)
    }

    override fun onComplete() {
        // do nothing
        //  Toasty.success(view.app,"completed").show()
    }

    private fun getErrorMessage(responseBody: ResponseBody): ErrorCustom {
        try {
            val jsonObject = JSONObject(responseBody.string())

            return ErrorCustom(
                jsonObject.getInt("statusCode"),
                jsonObject.getString("message")
            )

        } catch (e: Exception) {
            return ErrorCustom(HTTPStatus.MY_ERROR, e.message!!)
        }

    }
}