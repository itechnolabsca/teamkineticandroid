package base.har.retrofit

import base.har.base.BaseDataModel
import base.har.ui.testcoroutine.LoginServerCall
import io.reactivex.Observable
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface Apis {


    @POST(WebServicesConstants.LOGIN)
    fun login(@Body params: Any): Observable<BaseDataModel>

    @POST(WebServicesConstants.LOGIN)
    fun loginCoroutines(@Body  login:LoginServerCall): Deferred<Response<BaseDataModel>>

    @GET("movie/popular")
    fun getPopularMovie(): Deferred<Response<BaseDataModel>>

}