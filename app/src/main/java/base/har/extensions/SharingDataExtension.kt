package base.har.extensions

import android.content.Context
import android.content.Intent
import android.provider.Telephony
import base.har.R


fun sharingText(context: Context?) {

    context?.apply {
        val shareBody = "Hey There! I am using positionSelected fatntastic app for chatting and creating promises."
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Invite Keepook!")
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.shareUsing)))
    }
}


fun shareOnWhatsapp(context: Context?, smsNumber: String) {
    val sendIntent = Intent(Intent.ACTION_SEND)
    sendIntent.type = "text/plain"
    sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.")
    sendIntent.putExtra("jid", "$smsNumber@s.whatsapp.net") //phone number without "+" prefix
    sendIntent.setPackage("com.whatsapp")

    context?.startActivity(sendIntent)
}



val textToSend="Hey There! I am using positionSelected fantastic app for chatting and creating promises."
fun sharingOnMSG(context: Context?, phoneNumber: String) {

    val smsIntent = Intent(Intent.ACTION_VIEW)
    smsIntent.type = "vnd.android-dir/mms-sms"
    smsIntent.putExtra("address", phoneNumber)
    smsIntent.putExtra("sms_body", textToSend)

    try {
        context?.startActivity(smsIntent)
    } catch (e: Exception) {

        tryWay2(context, phoneNumber)
//        val defaultApplication = Settings.Secure.getString(context?.contentResolver, "sms_default_application")
//        val pm = context?.getPackageManager()
//        val intent = pm?.getLaunchIntentForPackage(defaultApplication)
//        if (intent != null) {
//            intent.putExtra("address", phoneNumber)
//            intent.putExtra("sms_body", textToSend)
//            context?.startActivity(intent)
//        }
    }
}

fun tryWay2(context: Context?, phoneNumber: String) {

    try{

//        val intent = Intent(getApplicationContext(), context)
//        val pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0)
//        val smgr = SmsManager.getDefault()
//        smgr.sendTextMessage(phoneNumber,null, textToSend,null,null)







    val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context) // Need to change the build to API 19
    val sendIntent = Intent(Intent.ACTION_SEND)
    sendIntent.type = "text/plain"
    sendIntent.putExtra(Intent.EXTRA_TEXT, textToSend)
   // sendIntent.data = Uri.parse("smsto: $phoneNumber");
    if (defaultSmsPackageName != null)
    // Can be null in case that there is no default, then the user would be able to choose
    // any app that support this intent.
    {
        sendIntent.setPackage(defaultSmsPackageName)
    }
    context?.startActivity(sendIntent)
    }
    catch (e:Exception ){
    }
}
