package base.har.extensions

import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.widget.Toast

 fun sendEmail(email: String,context:Context?) {
    val TO = arrayOf(email)

    val intent = Intent(Intent.ACTION_SEND)
    intent.putExtra(Intent.EXTRA_EMAIL, TO)
    intent.putExtra(Intent.EXTRA_SUBJECT, "hopNtrack")
    intent.type = "text/plain"
    val pm = context?.getPackageManager()!!
    val matches = pm.queryIntentActivities(intent, 0)
    var best: ResolveInfo? = null
    for (info in matches)
        if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
            best = info
    if (best != null)
        intent.setClassName(best.activityInfo.packageName, best.activityInfo.name)
    context?.startActivity(intent)


    try {
        context?.startActivity(intent)
    } catch (ex: android.content.ActivityNotFoundException) {
        Toast.makeText(context, "There is no G-mail client installed.", Toast.LENGTH_SHORT).show()
    }

}