package base.har.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri

import java.io.File
import java.io.IOException

object FileOpen {

    @Throws(IOException::class)
    fun openFile(context: Context, url: File) {
        // Create URI
        val uri = Uri.fromFile(url)

        val intent = Intent(Intent.ACTION_VIEW)
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (url.toString().contains(".doc",true) || url.toString().contains(".docx",true)) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if (url.toString().contains(".pdf",true)) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if (url.toString().contains(".ppt",true) || url.toString().contains(".pptx",true)) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint")
        } else if (url.toString().contains(".xls",true) || url.toString().contains(".xlsx",true)) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel")
        } else if (url.toString().contains(".zip",true) || url.toString().contains(".rar",true)) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav")
        } else if (url.toString().contains(".rtf",true)) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf")
        } else if (url.toString().contains(".wav",true) || url.toString().contains(".mp3",true)) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav")
        } else if (url.toString().contains(".gif",true)) {
            // GIF file
            intent.setDataAndType(uri, "image/gif")
        } else if (url.toString().contains(".jpg",true) || url.toString().contains(".jpeg",true) || url.toString().contains(".png",true)) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg")
        } else if (url.toString().contains(".txt",true)) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else if (url.toString().contains(".3gp",true) || url.toString().contains(".mpg",true) || url.toString().contains(".mpeg",true) || url.toString().contains(
                ".mpe",true
            ) || url.toString().contains(".mp4",true) || url.toString().contains(".avi",true)
        ) {
            // Video files
            intent.setDataAndType(uri, "video/*")
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }
}