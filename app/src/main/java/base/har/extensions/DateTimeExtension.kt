package base.har.extensions

import android.annotation.SuppressLint
import java.text.DateFormatSymbols
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun getDateFromMillies(toLong: Long): String {
    return SimpleDateFormat("dd MMM yy").format(toLong)
}

fun getTimeFromMillies(toLong: Long): String {
    return SimpleDateFormat("hh:mm a").format(toLong)
}

@SuppressLint("SimpleDateFormat")
fun getTime(min: Int): String? {
    var sdf = SimpleDateFormat("mm")

    try {
        val dt = sdf.parse("$min")
        sdf = SimpleDateFormat("hh:mm a")
        System.out.println(sdf.format(dt))
        return sdf.format(dt)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return "00:00"
}

fun getMonthData(calendar: Calendar): ArrayList<DaysData> {
    val currentCalendar = Calendar.getInstance()

    var maxDay:Int
    if((currentCalendar[Calendar.YEAR] == calendar[Calendar.YEAR] && currentCalendar[Calendar.MONTH] == calendar[Calendar.MONTH]))
    {

        maxDay = currentCalendar.get(Calendar.DATE)
    }
    else {
        maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        // Here we get the maximum days for the date specified
        // in the calendar. In this case we want to get the number
        // of days for Month
    }

    val list = ArrayList<DaysData>()
    for (i in 1..maxDay) {

        calendar.set(Calendar.DAY_OF_MONTH, i)
        val weekday = DateFormatSymbols.getInstance().weekdays[calendar[Calendar.DAY_OF_WEEK]]

        list.add(DaysData(weekday, i))

    }

    return list

}

fun currentDateWithoutMinutes(): String {

    val cal = Calendar.getInstance()
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)

    var format = "dd MMM yyyy"
    return SimpleDateFormat(format).format(cal.time)
}

fun currentDateWithMinutes(minutes: Int): String {

    val cal = Calendar.getInstance()
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, minutes)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)

    var format = "hh:mm a, dd MMM yyyy"
    return SimpleDateFormat(format).format(cal.time)
}

data class DaysData(
        var day: String,
        var date: Int
)


