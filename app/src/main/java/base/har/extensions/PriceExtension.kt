package base.har.extensions

fun getAmountSymbol(amount:Any?): String {

    if(amount==null || amount.toString().isBlank())
    {
        return "₹ $amount"
    }

    amount.apply {
        return "₹${"%.2f".format(amount.toString().toDouble())}"

    }

       return "₹ $amount"


}





