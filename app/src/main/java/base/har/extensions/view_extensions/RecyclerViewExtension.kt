package base.har.extensions.view_extensions

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView


fun RecyclerView.setBottomNavigationHideShow(bottomNavigation: BottomNavigationView) {

    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        var isAnimProgress = false
        var TOOLBAR_ELEVATION = 16
        // Keeps track of the overall vertical offset in the list
        internal var verticalOffset: Int = 0

        // Determines the scroll UP/DOWN direction
        internal var scrollingUp: Boolean = false

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            isAnimProgress=false
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                if (scrollingUp) {
                    hideBottomNavigationView(bottomNavigation)
                } else {
                    showBottomNavigationView(bottomNavigation)
                }
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            verticalOffset += dy
            if (isAnimProgress)
                return
            isAnimProgress = true
            scrollingUp = dy > 0
            if (scrollingUp) {
                hideBottomNavigationView(bottomNavigation)
            } else {
                showBottomNavigationView(bottomNavigation)
            }

            Log.e("dy", dy.toString() + "")

        }
    })


}


fun hideBottomNavigationView(view: BottomNavigationView) {
    view.animate().translationY(view.height.toFloat())
}

fun showBottomNavigationView(view: BottomNavigationView) {
    view.animate().translationY(0f)
}