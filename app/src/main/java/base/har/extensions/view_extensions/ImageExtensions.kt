package base.har.extensions.view_extensions

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.widget.ImageView
import base.har.R
import base.har.utils.CircleTransform
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_full_image.view.*
import java.io.File


  fun ImageView.setImage(context: Context, url: Uri) {
    Glide
        .with(this)
        .load(url)
        .fitCenter()
        .placeholder(R.drawable.placeholder)
//        .crossFade()
        .into(sdvAds)
}


fun ImageView.setImage(context: Context, url: String) {

    Glide
        .with(context)
        .load(url)
        .placeholder(R.drawable.placeholder)
        //.error(R.drawable.user_icon)
        .centerCrop()
        .into(this)
}


  fun ImageView.setImage(context: Context?, file: File) {
    Glide
        .with(context!!)
        .load(file)
        .placeholder(R.drawable.placeholder)
        //.error(R.drawable.user_icon)
        .into(this)
}


@SuppressLint("CheckResult")
  fun ImageView.setCircularImage(context: Context?, imageUrl: String?) {

   val requestOptions= RequestOptions()
    requestOptions.transform(CircleTransform())
    //requestOptions.error(R.drawable.user_icon)

    Glide.with(context!!)
        .load(imageUrl)
        .apply(requestOptions)
//        .transform(CircleTransform())
      //  .error(R.drawable.user_icon)
        .into(this)
}





fun ImageView.setImage(context: Context, url: String, width: Int, height: Int) {

    Glide
        .with(context)
        .load(url)
//           .apply(RequestOptions().override(width, height))
        .into(this)
}
