package base.har.base

import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import base.har.R
import base.har.cache.PrefNames
import base.har.cache.Prefs
import base.har.retrofit.ErrorCustom
import base.har.retrofit.HTTPStatus
import base.har.ui.SplashActivity
import base.har.utils.ProgressBarDialog
import es.dmoral.toasty.Toasty
import kotlinx.coroutines.Job

abstract class BaseViewModel(var app: Application) : AndroidViewModel(app), BaseView {


      var call: Job? = null

    var errorLiveData = MutableLiveData<ErrorCustom>()

    override fun isNetworkAvailable(): Boolean {
        return isNetworkAvailable()
    }

    fun getAccessToken(): String {
        return Prefs.with(app).getString(PrefNames.ACCESS_TOKEN, "")
    }

    override fun showLoading(message: String) {

        ProgressBarDialog.showProgressBar(app, "")
    }

    override fun hideLoading() {
        ProgressBarDialog.dismissProgressDialog()
    }

    override fun onUnknownError(error: String) {
        Toasty.error(app, error).show()
        hideLoading()
    }

    override fun onTimeout() {
        Toasty.error(app, app.getString(R.string.timeout)).show()
        hideLoading()
    }

    override fun onNetworkError() {
        throwError(ErrorCustom(999, app.getString(R.string.timeout)))
        //Toasty.error(app, app.getString(R.string.timeout)).show()
        hideLoading()
    }

    override fun onConnectionError() {
        Toasty.error(app, app.getString(R.string.timeout)).show()
        hideLoading()
    }

    fun throwError(error: ErrorCustom) {
        Toasty.error(app, error.message).show()
        errorLiveData.value = error
        hideLoading()

        if(error.statusCode== HTTPStatus.STATUS_INVALID_TOKEN)
        {

            Coroutines.ioThenMain({
                Prefs.with(app).removeAll()
            })
            {

                val intent = Intent(app, SplashActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                app.startActivity(intent)

            }
        }

    }


    override fun onCleared() {
        if (call != null && call?.isActive!!)
            call?.cancel()
        super.onCleared()
    }


}