package base.har.base

interface BaseView {

    fun isNetworkAvailable(): Boolean
    fun showLoading(message: String)

    fun hideLoading()

    fun onUnknownError(error: String)

    fun onTimeout()

    fun onNetworkError()

    fun onConnectionError()
}