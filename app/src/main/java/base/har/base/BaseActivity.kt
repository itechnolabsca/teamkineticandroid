package base.har.base


import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import base.har.R
import base.har.cache.PrefNames
import base.har.cache.Prefs
import base.har.utils.InternetCheck
import base.har.utils.ProgressBarDialog
import kotlinx.android.synthetic.main.main_toolbar_login.view.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


abstract class BaseActivity : AppCompatActivity(), BaseView {


    val keyBoardHeight: Int = 0

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    @IdRes
    protected fun getFragmentContainerId(): Int {
        return R.id.container
    }


    fun replaceFragment(@NonNull fragment: Fragment) {
        replaceFragment(fragment, false)
    }

    protected fun replaceFragment(@NonNull fragment: Fragment, addToBackStack: Boolean) {
        openFragment(this, fragment, true, true)
    }


    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    override fun showLoading(message: String) {
        ProgressBarDialog.showProgressBar(this, "")
    }

    fun getAccessToken(): String {
        return Prefs.with(this).getString(PrefNames.ACCESS_TOKEN, "")
    }

    override fun hideLoading() {
        ProgressBarDialog.dismissProgressDialog()

    }

    override fun onUnknownError(@NonNull error: String) {
        hideLoading()
        //Todo: show error view
    }

    override fun onNetworkError() {
        hideLoading()
        //Todo: show network error view
    }

    override fun onTimeout() {
        hideLoading()
    }

    override fun isNetworkAvailable(): Boolean {
        return InternetCheck.isConnectedToInternet(this)
    }

    override fun onConnectionError() {
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var fragment = getCurrentFragment()

       when(fragment)
       {
//           is LoginSignupFragment ->{
//               fragment.facebook.callbackManager?.onActivityResult(requestCode, resultCode, data)
//           }
//
//           is CreatePromiseFragment,is PopAllFilterFragment->{
//               fragment.onActivityResult(requestCode, resultCode, data)
           }

       }




    private fun getCurrentFragment(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.container)
    }


    fun showAlert(message: String, buttonText: String, onClick: () -> Unit) {

        var dialog = AlertDialog.Builder(this)
        dialog.setMessage(message)
        dialog.setCancelable(false)
        dialog.setPositiveButton(buttonText) { dialog, which ->
            onClick()
            dialog.dismiss()
        }

        dialog.show()

    }


    fun getCurrentFrag(): Fragment? {
        return supportFragmentManager.findFragmentById(R.id.container)

    }


    fun hideKeyBoard(input: View?) {

        input?.let {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(input.windowToken, 0)
        }
    }


    fun setToolbar(name: String, drawable: Int?, toolbar: Toolbar) {

            if (name.isNotEmpty())
                toolbar.tvTitle.setCompoundDrawables(null, null, null, null)
            toolbar.tvTitle.text = name

            toolbar.navigationIcon?.setTint(ContextCompat.getColor(this@BaseActivity,  R.color.colorPrimary))
            if (drawable != null)
                toolbar.setNavigationIcon(drawable)
            toolbar.setNavigationOnClickListener {
                hideKeyBoard(toolbar)
                onBackPressed()
            }

    }
}




