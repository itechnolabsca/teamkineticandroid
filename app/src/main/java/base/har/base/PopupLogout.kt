package base.har.base

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import base.har.R
import base.har.cache.Prefs
import base.har.ui.SplashActivity
import kotlinx.android.synthetic.main.dialog_action.*

@SuppressLint("ValidFragment")
class PopupLogout() : BaseDialogFragment(), View.OnClickListener {

     override fun getContentLayoutResId(): Int = R.layout.dialog_action

    override fun onActivityCreated(arg0: Bundle?) {
        super.onActivityCreated(arg0)
        dialog?.window!!.attributes.windowAnimations = R.style.DialogAnimation
        tvTitle.text = getString(R.string.logout)
        tvMessage.text = getString(R.string.logoutMessage)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
    }


    private fun listeners() {
        btnPositive.setOnClickListener(this)
        btnNegative.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when (v) {
            btnPositive -> {

                Coroutines.ioThenMain({
                    Prefs.with(context).removeAll()
                })
                {

                    val intent = Intent(activity, SplashActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    activity?.finish()
                    dismiss()
                }


            }

            btnNegative -> {
                dismiss()
            }
        }
    }




}