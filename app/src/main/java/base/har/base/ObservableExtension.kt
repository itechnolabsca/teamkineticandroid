package base.har.base

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T> Observable<T>.setObserver(): Observable<T> {
    return observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
}