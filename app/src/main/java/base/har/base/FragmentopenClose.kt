package base.har.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import base.har.R


fun openFragment(act: FragmentActivity?, fragment: Fragment, shouldAnimate: Boolean, shouldAdd: Boolean) {

    act?.apply {
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()

        if (shouldAnimate) {
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

        }

        ft.replace(R.id.container, fragment, fragment::class.java.canonicalName)

        if (shouldAdd)
            ft.addToBackStack(fragment::class.java.canonicalName)

        ft.commit()
    }

}



fun openFragmentWithContainer(container:Int,act: FragmentActivity?, fragment: Fragment, shouldAnimate: Boolean, shouldAdd: Boolean) {

    act?.apply {
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()

        if (shouldAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)

        }

        ft.replace(container, fragment, fragment::class.java.canonicalName)

        if (shouldAdd)
            ft.addToBackStack(fragment::class.java.canonicalName)

        ft.commit()
    }

}


fun openFragmentBottom(act: FragmentActivity?, fragment: Fragment, shouldAnimate: Boolean, shouldAdd: Boolean) {

    act?.apply {
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()

        if (shouldAnimate) {
//            if (getLanguage(act) == LANGUAGE_ENGLISH) {
                ft.setCustomAnimations(R.anim.slide_up_dialogue, R.anim.slide_out_up, R.anim.slide_up_dialogue, R.anim.slide_out_up)
//            } else {
//                ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
//            }
        }

        ft.replace(R.id.container, fragment, fragment::class.java.canonicalName)

        if (shouldAdd)
            ft.addToBackStack(fragment::class.java.canonicalName)

        ft.commit()
    }

}


fun addFragment(act: FragmentActivity?, fragment: Fragment) {

    act?.apply {
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()

        ft.add(R.id.container, fragment, fragment::class.java.canonicalName)

        ft.commit()
    }

}


fun getFragmentById(act: FragmentActivity?, fragmentId: Int): Fragment? {
    return act?.supportFragmentManager!!.findFragmentById(fragmentId)
}

fun getFragmentByTag(act: FragmentActivity?, fragmentTag: String): Fragment? {
       return act?.supportFragmentManager!!.findFragmentByTag(fragmentTag)
}

fun addHideFragment(act: FragmentActivity?, fragmentHide: Fragment) {
    act?.apply {
        supportFragmentManager!!.beginTransaction().add(R.id.container,fragmentHide).hide(fragmentHide).commit()
    }
}

fun hideOpenFragment(act: FragmentActivity?, fragmentHide: Fragment, fragmentOpen: Fragment) {
    act?.apply {

if(fragmentHide==fragmentOpen)
    supportFragmentManager!!.beginTransaction().show(fragmentOpen).commit()
    else
        supportFragmentManager!!.beginTransaction().hide(fragmentHide).show(fragmentOpen).commit()
    }
}