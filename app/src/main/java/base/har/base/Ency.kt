package base.har.base

import android.util.Base64

import java.security.MessageDigest

import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

object Ency {


    private val secret = "passwordHere"
    internal var encryptedValue = ""

    fun encrypt(value: String): String? {
        try {
            val key = generateKey(secret)

            val cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.ENCRYPT_MODE, key)

            val encrypted = cipher.doFinal(value.toByteArray())
            return Base64.encodeToString(encrypted, Base64.DEFAULT)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return null
    }


    fun decrypt(encrypted: String): String? {
        try {
            val key = generateKey(secret)

            val cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.DECRYPT_MODE, key)

            val decodeValue = Base64.decode(encryptedValue, Base64.DEFAULT)
            val original = cipher.doFinal(decodeValue)

            return String(original)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return null
    }

    @Throws(Exception::class)
    private fun generateKey(password: String): SecretKeySpec {
        val digest = MessageDigest.getInstance("SHA-256")
        val bytes = password.toByteArray(charset("UTF-8"))
        digest.update(bytes, 0, bytes.size)
        val key = digest.digest()
        return SecretKeySpec(key, "AES")
    }

}
