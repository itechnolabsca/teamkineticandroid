package base.har.cache

import android.app.Activity
import android.content.Context

//fun getProfile(context:Context?): LoginData? {
//   context?.apply {
//      return Prefs.with(context)
//         .getObject(PrefNames.USER_PROFILE_DATA, LoginData::class.java)
//   }
//   return null
//}


fun getAccessToken(context:Context?):String?
{
   if(checkifNull(context)) return null
   return Prefs.with(context).getString(PrefNames.ACCESS_TOKEN, null)
}




fun getDeviceToken(context:Context?):String?
{
   if(checkifNull(context)) return null
   return Prefs.with(context).getString(PrefNames.PUSH_TOKEN, null)
}


fun clearData(act: Activity) {
   var deviceToken= Prefs.with(act).getString(PrefNames.PUSH_TOKEN,"")
   Prefs.with(act).removeAll()
   Prefs.with(act).save(PrefNames.PUSH_TOKEN, deviceToken)

}

fun checkifNull(context: Any?): Boolean {
   return context==null
}