package base.har.cache


object PrefNames {
    const val BOOT_TIME_SYSTEM = "bootTimeSystem"
    const val SERVER_TIME = "serverTime"
    const val PUSH_TOKEN = "pushToken"
    const val ACCESS_TOKEN = "accessToken"
}
