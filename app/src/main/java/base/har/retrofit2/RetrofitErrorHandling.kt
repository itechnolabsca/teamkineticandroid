package base.har.retrofit2

import android.util.Log
import base.har.base.BaseDataModel
import base.har.base.BaseViewModel
import base.har.retrofit.ErrorCustom
import base.har.retrofit.HTTPStatus
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response

object RetrofitErrorHandling {

    fun isCallSuccess(response: Any?): Boolean {

        if (response !is Response<*>) return false

        if (response.body() == null && response.errorBody() != null)
            return false


        return true
    }


    fun getErrorMessage(responseBody: Any?): ErrorCustom {


        if (responseBody !is Response<*> || responseBody.errorBody() == null) {
            return ErrorCustom(HTTPStatus.MY_ERROR, "Something went wrong,Please try again")
        }

        try {
            val jsonObject = JSONObject(responseBody.errorBody()!!.string())

            return ErrorCustom(
                jsonObject.getInt("statusCode"),
                jsonObject.getString("message")
            )

        } catch (e: Exception) {
            return ErrorCustom(HTTPStatus.MY_ERROR, e.message!!)
        }

    }


    fun getHttpErrorMessage(exception: HttpException): ErrorCustom {
        val gson = Gson()
        var error = ErrorCustom()
        val adapter = gson.getAdapter(ErrorCustom::class.java)
        try {
            val errorBody = exception.response().errorBody()
            if (exception.response().errorBody() != null)
                error = adapter.fromJson(
                    errorBody?.string()
                )
            Log.e("API ERROR ", error.message)
            error.statusCode = error.statusCode
            return error
        } catch (e: Exception) {
            e.printStackTrace()
            return somethingWentWrong(exception)
        }
    }


    fun getSuccessData(result: Any, viewModel: BaseViewModel): BaseDataModel? {

        if (result is ErrorCustom) {
            viewModel.errorLiveData.value = result
            return null
        } else if (!isCallSuccess(result)) {
            viewModel.errorLiveData.value = getErrorMessage(result)
            return null
        } else
        return (result as Response<BaseDataModel>).body()

    }

}


fun somethingWentWrong(exception: HttpException): ErrorCustom {
    var error = ErrorCustom()
    error.message = exception.message()
    error.statusCode == exception.code()

    return error
}